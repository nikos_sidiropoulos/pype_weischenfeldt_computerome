import os
import re
import shlex
import subprocess
from distutils.spawn import find_executable
from pype.env_modules import get_module_cmd, program_string
from pype.misc import xopen


def requirements():
    return({'ncpu': 1, 'time': '800:00:00', 'mem': '1gb'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    try:
        win = argv['--bin']
    except KeyError:
        win = argv['-w']
    except KeyError:
        win = 50
    output = re.sub('.gz$', '', output)
    output = re.sub('.seqz$', '', output)
    output = re.sub('.sqz$', '', output)
    output = '%s_bin%i' % (output, int(win))
    return({'seqz': '%s.seqz.gz' % output,
            'idx': '%s.seqz.gz.tbi' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Merge multiple seqz files '
                                       '(usually from different '
                                       'chromosomes) to a binned seqz file'),
                                 add_help=False)


def merge_seqz_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='input', nargs='*',
                        help='Normal sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output seqz file prefix', required=True)
    parser.add_argument('-w', '--bin', dest='bin', default=50, type=int,
                        help='Size of binned windows')
    return parser.parse_args(argv)


def merge_seqz(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = merge_seqz_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['tabix']))
    module('add', program_string(profile.programs['samtools_1']))
    module('add', program_string(profile.programs['sequenza-utils']))

    output = re.sub('.gz$', '', args.out)
    output = re.sub('.seqz$', '', output)
    output = re.sub('.sqz$', '', output)
    output = '%s_bin%i' % (output, args.bin)
    log.log.info('Adjust output file from %s to %s' % (args.out, output))

    seqz_files = list()
    log.log.info('Check if all files in input are present')
    for seqz_file in args.input:
        if os.path.isfile(seqz_file):
            seqz_files.append(seqz_file)
        else:
            log.log.warning('File %s was not found' % seqz_file)

    log.log.info('Preparing merge_seqz command line')
    seqz_cmd = ['sequenza-utils', 'seqz_binning', '--seqz', '-',
                '--window', args.bin, '-o', '%s.seqz.gz' % output]

    seqz_cmd = shlex.split(' '.join(map(str, seqz_cmd)))

    log.log.info(' '.join(map(str, seqz_cmd)))
    log.log.info('Execute sequenza-utils with python subprocess.Popen')
    sequenza_proc = subprocess.Popen(seqz_cmd, stdin=subprocess.PIPE)
    log.log.info('Open first file (%s) in input to read the seqz header'
                 % args.input[0])
    with xopen(args.input[0], 'rt') as seqz:
        sequenza_proc.stdin.write(next(seqz))

    for seqz_file in seqz_files:
        log.log.info('Pipe file %s content' % seqz_file)
        cat_seqz(seqz_file, sequenza_proc.stdin, log)
        #with xopen(seqz_file, 'rt') as seqz:
        #    header = next(seqz)
        #    for line in seqz:
        #        sequenza_proc.stdin.write(line)
    log.log.info('Close input pipe')
    sequenza_proc.stdin.close()
    out0 = sequenza_proc.communicate()[0]

    log.log.info('Terminate merge_seqz')


def cat_seqz(file_name, stdout, log):
    if file_name.endswith('.gz'):
        cat_cmd = find_executable('gzcat')
        if cat_cmd is None:
            log.log.warning('gzcat not found, trying zcat')
            cat_cmd = find_executable('zcat')
        if cat_cmd is None:
            log.log.error('gzcat/zcat command not found')
            raise Exception(('Impossiblo to find (g)zcat '
                             'to decompress %s') % file_name)
    else:
        cat_cmd = 'cat'
    cat_cmd = '%s %s' % (cat_cmd, file_name)
    tail_cmd = 'tail -n +2'
    log.log.info("%s | %s" % (cat_cmd, tail_cmd))
    cat_cmd = shlex.split(cat_cmd)
    tail_cmd = shlex.split(tail_cmd)
    cat_proc = subprocess.Popen(cat_cmd, stdout=subprocess.PIPE)
    tail_proc = subprocess.Popen(tail_cmd, stdin=cat_proc.stdout,
                                 stdout=stdout)
    cat_proc.stdout.close()
    tail_proc.communicate() 
