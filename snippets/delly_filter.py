import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '12:00:00'})


def results(argv):
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = re.sub('.bcf$', '', output)
    output = re.sub('.gz$', '', output)
    output = re.sub('.vcf$', '', output)
    return({'vcf': '%s.vcf.gz' % output, 'tbi': '%s.vcf.gz.tbi' % output,
            'bedpe': '%s.bedpe.txt.gz' % output,
            'bedpe_genes': '%s.bedpe.genes.txt.gz' % output,
            'germline_vcf': '%s.germline.vcf.gz' % output,
            'germline_tbi': '%s.germline.vcf.gz.tbi' % output,
            'germline_bedpe': '%s.germline.bedpe.txt.gz' % output,
            'germline_bedpe_genes': '%s.germline.bedpe.genes.txt.gz' % output,
            'germline_highConf_vcf': '%s.germline.highConf.vcf.gz' % output,
            'germline_highConf_tbi':
                '%s.germline.highConf.vcf.gz.tbi' % output,
            'germline_highConf_bedpe':
                '%s.germline.highConf.bedpe.txt.gz' % output,
            'germline_highConf_bedpe_genes':
                '%s.germline.highConf.bedpe.genes.txt.gz' % output,
            'somatic_vcf': '%s.somatic.vcf.gz' % output,
            'somatic_tbi': '%s.somatic.vcf.gz.tbi' % output,
            'somatic_bedpe': '%s.somatic.bedpe.txt.gz' % output,
            'somatic_bedpe_genes': '%s.somatic.bedpe.genes.txt.gz' % output,
            'somatic_highConf_vcf': '%s.somatic.highConf.vcf.gz' % output,
            'somatic_highConf_tbi': '%s.somatic.highConf.vcf.gz.tbi' % output,
            'somatic_highConf_bedpe':
                '%s.somatic.highConf.bedpe.txt.gz' % output,
            'somatic_highConf_bedpe_genes':
                '%s.somatic.highConf.bedpe.genes.txt.gz' % output})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Combine and filter delly '
                                       'structural variants results'),
                                 add_help=False)


def delly_filter_args(parser, subparsers, argv):
    parser.add_argument('-i', '--input', dest='vcfs', nargs='*',
                        help='List of VCF/BCF files to merge and filter',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output VCF file', required=True)
    return parser.parse_args(argv)


def delly_filter(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = delly_filter_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['tabix']))
    module('add', program_string(profile.programs['bcftools']))
    module('add', program_string(profile.programs['bedtools']))
    module('add', program_string(profile.programs['delly-filter']))

    gencode = profile.files['gencode_bed']
    log.log.info('Use gencode bed file %s' % gencode)

    gen_len = profile.files['genome_len']
    log.log.info('Use genome lengths file %s' % gen_len)

    output = re.sub('.bcf$', '', args.out)
    output = re.sub('.gz$', '', output)
    output = re.sub('.vcf$', '', output)
    output_merged = '%s.vcf.gz' % output

    output_dir, output_file = os.path.split(os.path.abspath(output))
    if not os.path.exists(output_dir):
        log.log.info('Create output directory %s' % output_dir)
        os.makedirs(output_dir)

    log.log.info('Preparing bcftools concat command line')
    merge_cmd = ['bcftools', 'concat',
                 '--allow-overlaps', '-O', 'v'] + args.vcfs
    bgzip_cmd = ['bgzip', '-c']
    tabix_cmd = ['tabix', '-p', 'vcf', '-f', output_merged]
    filter_cmd = ['DellySomaticFreqFilter.py', '-v', output_merged]
    vcf2tsv_cmd = ['dellyVcf2Tsv.py', '-v', output_merged,
                   '-o', '%s.bedpe.txt' % output, '-g', gencode, gen_len]
    ziptsv_cmd = ['bgzip', '-f', '%s.bedpe.txt' % output]
    ziptsvg_cmd = ['bgzip', '-f', '%s.bedpe.genes.txt' % output]

    filter_out = ['%s.somatic.vcf', '%s.somatic.highConf.vcf',
                  '%s.germline.vcf', '%s.germline.highConf.vcf']
    filter_out = [i % output for i in filter_out]

    merge_cmd = shlex.split(' '.join(map(str, merge_cmd)))
    bgzip_cmd = shlex.split(' '.join(map(str, bgzip_cmd)))
    tabix_cmd = shlex.split(' '.join(map(str, tabix_cmd)))
    filter_cmd = shlex.split(' '.join(map(str, filter_cmd)))
    vcf2tsv_cmd = shlex.split(' '.join(map(str, vcf2tsv_cmd)))
    ziptsv_cmd = shlex.split(' '.join(map(str, ziptsv_cmd)))
    ziptsvg_cmd = shlex.split(' '.join(map(str, ziptsvg_cmd)))

    log.log.info('Open file %s to write merged vcf output' % output_merged)
    with open(output_merged, 'wt') as merged_vcf:
        log.log.info('Execute bcftools concat with python subprocess.Popen')
        log.log.info('%s | %s > %s' % (' '.join(map(str, merge_cmd)),
                                       ' '.join(map(str, bgzip_cmd)),
                                       output_merged))
        merge_proc = subprocess.Popen(merge_cmd, stdout=subprocess.PIPE)
        bgzip_proc = subprocess.Popen(bgzip_cmd, stdin=merge_proc.stdout,
                                      stdout=merged_vcf)
        merge_proc.stdout.close()
        out0 = bgzip_proc.communicate()[0]

    log.log.info('Index merged file %s' % output_merged)
    log.log.info(' '.join(map(str, tabix_cmd)))
    tabix_proc = subprocess.Popen(tabix_cmd)
    out2 = tabix_proc.communicate()[0]

    log.log.info('Convert VCF to bedpe format %s' % output_merged)
    log.log.info(' '.join(map(str, vcf2tsv_cmd)))
    bedpe_proc = subprocess.Popen(vcf2tsv_cmd)
    out3 = bedpe_proc.communicate()[0]

    log.log.info('Compress bedpe format %s.bedpe.txt.gz' % output)
    log.log.info(' '.join(map(str, ziptsv_cmd)))
    ziptsv_proc = subprocess.Popen(ziptsv_cmd)
    out4 = ziptsv_proc.communicate()[0]
    log.log.info('Compress bedpe format %s.bedpe.genes.txt.gz' % output)
    log.log.info(' '.join(map(str, ziptsvg_cmd)))
    ziptsvg_proc = subprocess.Popen(ziptsvg_cmd)
    out5 = ziptsvg_proc.communicate()[0]

    log.log.info('Apply filter to %s' % output_merged)
    log.log.info(' '.join(map(str, filter_cmd)))
    filter_proc = subprocess.Popen(filter_cmd)
    out6 = filter_proc.communicate()[0]

    for file in filter_out:
        bgzip_i_cmd = ['bgzip', '-f', file]
        tabix_i_cmd = ['tabix', '-p', 'vcf', '-f', '%s.gz' % file]
        file_prefix = re.sub('.vcf$', '', file)
        vcf2tsv_i_cmd = ['dellyVcf2Tsv.py', '-v', file,
                         '-o', '%s.bedpe.txt' % file_prefix, '-g', gencode, gen_len]
        ziptsv_i_cmd = ['bgzip', '-f', '%s.bedpe.txt' % file_prefix]
        ziptsvg_i_cmd = ['bgzip', '-f', '%s.bedpe.genes.txt' % file_prefix]

        bgzip_i_cmd = shlex.split(' '.join(map(str, bgzip_i_cmd)))
        tabix_i_cmd = shlex.split(' '.join(map(str, tabix_i_cmd)))
        vcf2tsv_i_cmd = shlex.split(' '.join(map(str, vcf2tsv_i_cmd)))
        ziptsv_i_cmd = shlex.split(' '.join(map(str, ziptsv_i_cmd)))
        ziptsvg_i_cmd = shlex.split(' '.join(map(str, ziptsvg_i_cmd)))

        log.log.info('Convert %s to bedpe format' % file)
        log.log.info(' '.join(map(str, vcf2tsv_i_cmd)))
        vcf2tsv_i_proc = subprocess.Popen(vcf2tsv_i_cmd)
        out_2i = vcf2tsv_i_proc.communicate()[0]
        log.log.info(' '.join(map(str, ziptsv_i_cmd)))
        ziptsv_i_proc = subprocess.Popen(ziptsv_i_cmd)
        out_3i = ziptsv_i_proc.communicate()[0]
        log.log.info(' '.join(map(str, ziptsvg_i_cmd)))
        ziptsvg_i_proc = subprocess.Popen(ziptsvg_i_cmd)
        out_3i = ziptsvg_i_proc.communicate()[0]

        log.log.info('Compress and index %s' % file)
        log.log.info(' '.join(map(str, bgzip_i_cmd)))
        bgzip_i_proc = subprocess.Popen(bgzip_i_cmd)
        out_0i = bgzip_i_proc.communicate()[0]
        log.log.info(' '.join(map(str, tabix_i_cmd)))
        tabix_i_proc = subprocess.Popen(tabix_i_cmd)
        out_1i = tabix_i_proc.communicate()[0]

    log.log.info('Terminate delly_filter')
