import os
import shlex
import subprocess
from pype.misc import generate_uid
from pype.env_modules import get_module_cmd, program_string


def requirements():
    return({'ncpu': 1, 'time': '48:00:00'})


def results(argv):
    try:
        path = argv['--out']
    except KeyError:
        path = argv['-o']

    return({'vcf': os.path.join(path, 'mutect_%s.vcf' % argv['--sample']),
            'out': os.path.join(path, 'mutect_%s.out' % argv['--sample']),
            'coverage': os.path.join(path, 'mutect_%s.coverage' % argv['--sample'])})


def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name, help='Call germline and somatic variants with mutect', add_help=False)


def mutect_args(parser, subparsers, argv):
    parser.add_argument('-n', '--normal', dest='normal',
                        help='Normal BAM file', required=True)
    parser.add_argument('-t', '--tumor', dest='tumor',
                        help='Tumor BAM file', required=True)
    parser.add_argument('--initial-lod', dest='ilod', type=float,
                        help='Initial LOD threshold for calling tumor variant', default=4.0)
    parser.add_argument('--lod', dest='lod', type=float,
                        help='LOD threshold for calling tumor variant', default=6.3)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output directory', required=True)
    parser.add_argument('--sample', dest='sample',
                        help='Sample name used to write the file name', required=True)
    return parser.parse_args(argv)


def mutect(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = mutect_args(add_parser(subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools', 'ngs')
    module('add', program_string(profile.programs['java7']))
    module('add', program_string(profile.programs['mutect']))

    genome = profile.files['genome_fa']
    cosmic = profile.files['cosmic']
    dbSNP = profile.files['dbSNP']
    exons = profile.files['exons_list']
    log.log.info('Use genome file %s' % genome)
    log.log.info('Use Cosmic DB at %s' % cosmic)
    log.log.info('Use dbSNP DB at %s' % dbSNP)
    log.log.info('Use exon list at %s' % exons)

    log.log.info('Preparing mutect command line')
    env = dict(os.environ)
    paths = env['PATH'].split(':')
    for path in paths:
        if 'mutect' in path:
            mutect_path = path
            break
    mutect_jar = os.path.join(mutect_path, 'mutect-%s.jar' %
                              profile.programs['mutect']['version'])
    log.log.info('mutect_path %s' % mutect_jar)
    mutect_cmd = ['java', '-cp', mutect_path, '-jar', mutect_jar, '--analysis_type', 'MuTect', '--reference_sequence', genome,
                  '--intervals', exons, '--input_file:normal', args.normal, '--input_file:tumor', args.tumor,
                  '--out', os.path.join(args.out, 'mutect_%s.out' %
                                        args.sample), '--cosmic', cosmic, '--dbsnp', dbSNP, '--tumor_lod', args.lod,
                  '--initial_tumor_lod', args.ilod, '--coverage_file', os.path.join(
                      args.out, 'mutect_%s.coverage' % args.sample),
                  '--vcf', os.path.join(args.out, 'mutect_%s.vcf' % args.sample)]
    mutect_cmd = shlex.split(' '.join(map(str, mutect_cmd)))
    args.out
    if not os.path.exists(args.out):
        log.log.info('Create a new directory %s' % args.out)
        os.makedirs(args.out)
    log.log.info(' '.join(map(str, mutect_cmd)))
    log.log.info('Execute mutect python subprocess.Popen')
    mutect_proc = subprocess.Popen(mutect_cmd)

    out0 = mutect_proc.communicate()[0]
    log.log.info('Terminate mutect')
